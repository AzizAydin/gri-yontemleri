﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPM411
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void pikselAlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pikselFr pikselFormu = new pikselFr();
            pikselFormu.ShowDialog();
        }

        private void invertToolStripMenuItem_Click(object sender, EventArgs e)
        {
            invertFr invertFormu = new invertFr();
            invertFormu.ShowDialog();

        }

        private void griToolStripMenuItem_Click(object sender, EventArgs e)
        {
            griFr griFormu = new griFr();
            griFormu.ShowDialog();
        }

        private void parlaklıkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            parlaklikFr parlaklikFormu = new parlaklikFr();
            parlaklikFormu.ShowDialog();
        }
    }
}
