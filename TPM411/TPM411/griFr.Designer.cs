﻿namespace TPM411
{
    partial class griFr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dosyaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.açToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.griYöntemleriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ortalamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bT709ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lUMAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aCIKLIKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tEKRENKKANALIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizeEdilmisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.islemBox = new System.Windows.Forms.PictureBox();
            this.kaynakBox = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.islemBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kaynakBox)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dosyaToolStripMenuItem,
            this.griYöntemleriToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1151, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dosyaToolStripMenuItem
            // 
            this.dosyaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.açToolStripMenuItem});
            this.dosyaToolStripMenuItem.Name = "dosyaToolStripMenuItem";
            this.dosyaToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.dosyaToolStripMenuItem.Text = "Dosya";
            // 
            // açToolStripMenuItem
            // 
            this.açToolStripMenuItem.Name = "açToolStripMenuItem";
            this.açToolStripMenuItem.Size = new System.Drawing.Size(88, 22);
            this.açToolStripMenuItem.Text = "Aç";
            this.açToolStripMenuItem.Click += new System.EventHandler(this.açToolStripMenuItem_Click);
            // 
            // griYöntemleriToolStripMenuItem
            // 
            this.griYöntemleriToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ortalamaToolStripMenuItem,
            this.bT709ToolStripMenuItem,
            this.lUMAToolStripMenuItem,
            this.aCIKLIKToolStripMenuItem,
            this.tEKRENKKANALIToolStripMenuItem,
            this.normalizeEdilmisToolStripMenuItem});
            this.griYöntemleriToolStripMenuItem.Name = "griYöntemleriToolStripMenuItem";
            this.griYöntemleriToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.griYöntemleriToolStripMenuItem.Text = "Gri Yöntemleri";
            // 
            // ortalamaToolStripMenuItem
            // 
            this.ortalamaToolStripMenuItem.Name = "ortalamaToolStripMenuItem";
            this.ortalamaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ortalamaToolStripMenuItem.Text = "Ortalama";
            this.ortalamaToolStripMenuItem.Click += new System.EventHandler(this.ortalamaToolStripMenuItem_Click);
            // 
            // bT709ToolStripMenuItem
            // 
            this.bT709ToolStripMenuItem.Name = "bT709ToolStripMenuItem";
            this.bT709ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.bT709ToolStripMenuItem.Text = "BT-709";
            this.bT709ToolStripMenuItem.Click += new System.EventHandler(this.bT709ToolStripMenuItem_Click);
            // 
            // lUMAToolStripMenuItem
            // 
            this.lUMAToolStripMenuItem.Name = "lUMAToolStripMenuItem";
            this.lUMAToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.lUMAToolStripMenuItem.Text = "LUMA";
            this.lUMAToolStripMenuItem.Click += new System.EventHandler(this.lUMAToolStripMenuItem_Click);
            // 
            // aCIKLIKToolStripMenuItem
            // 
            this.aCIKLIKToolStripMenuItem.Name = "aCIKLIKToolStripMenuItem";
            this.aCIKLIKToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.aCIKLIKToolStripMenuItem.Text = "ACIKLIK";
            this.aCIKLIKToolStripMenuItem.Click += new System.EventHandler(this.aCIKLIKToolStripMenuItem_Click);
            // 
            // tEKRENKKANALIToolStripMenuItem
            // 
            this.tEKRENKKANALIToolStripMenuItem.Name = "tEKRENKKANALIToolStripMenuItem";
            this.tEKRENKKANALIToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.tEKRENKKANALIToolStripMenuItem.Text = "TEK RENK KANALI";
            this.tEKRENKKANALIToolStripMenuItem.Click += new System.EventHandler(this.tEKRENKKANALIToolStripMenuItem_Click);
            // 
            // normalizeEdilmisToolStripMenuItem
            // 
            this.normalizeEdilmisToolStripMenuItem.Name = "normalizeEdilmisToolStripMenuItem";
            this.normalizeEdilmisToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.normalizeEdilmisToolStripMenuItem.Text = "Normalize Edilmis";
            this.normalizeEdilmisToolStripMenuItem.Click += new System.EventHandler(this.normalizeEdilmisToolStripMenuItem_Click);
            // 
            // islemBox
            // 
            this.islemBox.Location = new System.Drawing.Point(545, 40);
            this.islemBox.Name = "islemBox";
            this.islemBox.Size = new System.Drawing.Size(460, 480);
            this.islemBox.TabIndex = 4;
            this.islemBox.TabStop = false;
            // 
            // kaynakBox
            // 
            this.kaynakBox.Location = new System.Drawing.Point(30, 40);
            this.kaynakBox.Name = "kaynakBox";
            this.kaynakBox.Size = new System.Drawing.Size(460, 480);
            this.kaynakBox.TabIndex = 3;
            this.kaynakBox.TabStop = false;
            // 
            // griFr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1151, 634);
            this.Controls.Add(this.islemBox);
            this.Controls.Add(this.kaynakBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "griFr";
            this.Text = "griFr";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.islemBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kaynakBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dosyaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem açToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem griYöntemleriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ortalamaToolStripMenuItem;
        private System.Windows.Forms.PictureBox islemBox;
        private System.Windows.Forms.PictureBox kaynakBox;
        private System.Windows.Forms.ToolStripMenuItem bT709ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lUMAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aCIKLIKToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tEKRENKKANALIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalizeEdilmisToolStripMenuItem;
    }
}